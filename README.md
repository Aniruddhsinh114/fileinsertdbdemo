# FileInsertDbDemo

Demo project for uploading file to database.

This document describing rest api with response,including method type and parameters.

**Prefix URL**

http://{host}:{port}/

**For Example**

http://localhost:8080/

**Files**

The File resource represents a file with all information.

| Property | Description |
| ------ | ------ |
| id | The unique identifier by which to identify the file |
| filename | Name of file | 
| type | Type of file |
| file | Multipart file |
| createdAt | Creation date and time on create |
| updatedAt | Updation date and time on update  |

**List Resource**

/api/v1/files

**GET**

Returns a list of all files

GET /api/v1/files 

```
[
  {
    "id": 1,
    "filename": "backup.sql",
    "type": "application/x-sql",
    "file": "LS0gTXlTUUwgZHVtcCAxMC4xMyAgRGlzdHJpYiA1LjcuMjUsIGZvciBMaW51eCAoeDg2XzY0KQotLQot0tIER1bXAgY29tcGxldGVkIG9uIDIwMTktMDMtMjQgMTU6NTg6MDEK",
    "createdAt": "2019-03-25T08:43:48.000+0000",
    "updatedAt": "2019-03-25T08:57:53.000+0000",
    "primaryKey": {
      "id": 1
    }
  },
  {
    "id": 2,
    "filename": "examples.desktop",
    "type": "application/octet-stream",
    "file": "bc3ddPUJpZGhhYSBtZmFubyB5YSBVYnVudHUKQ29tbWVudFtzemxdPUJhanN6cGlsbsWPIHRyZcWbxIcgZGzFjyBVYnVudHUKQ29tbWVudFt0YV094K6J4K6q4K+B4K6j4K+N4K6f4K+B4K614K6/4K6x4K+N4K6V4K6+4K6pIOCujuCun+CvgeCupOCvjeCupOCvgeCuleCuvuCun+CvjeCun+CvgSDgrongrrPgr43grrPgrp",
    "createdAt": "2019-03-25T18:20:57.000+0000",
    "updatedAt": "2019-03-25T18:20:57.000+0000",
    "primaryKey": {
      "id": 2
    }
  }
]
```
**Individual File**

The file according to id.

| Property | Description |
| ------ | ------ |
| id | The unique identifier by which to identify the file |

File Resource

/api/v1/files/{id}

**GET**

Returns single file

GET /api/v1/files /{id}

 ```
{
    "id": 1,
    "filename": "backup.sql",
    "type": "application/x-sql",
    "file": "LS0gTXlTUUwgZHVtcCAxMC4xMyAgRGlzdHJpYiA1LjcuMjUsIGZvciBMaW51eCAoeDg2XzY0KQotLQot0tIER1bXAgY29tcGxldGVkIG9uIDIwMTktMDMtMjQgMTU6NTg6MDEK",
    "createdAt": "2019-03-25T08:43:48.000+0000",
    "updatedAt": "2019-03-25T08:57:53.000+0000",
    "primaryKey": {
      "id": 1
    }
  }
```

**Store File**

Storing single file

| Property | Description |
| ------ | ------ |
| file | Multipart file |

**File Resource**

/api/v1/files/

**POST**

POST /api/v1/files

 ```
{
    "id": 1,
    "filename": "backup.sql",
    "type": "application/x-sql",
    "file": "LS0gTXlTUUwgZHVtcCAxMC4xMyAgRGlzdHJpYiA1LjcuMjUsIGZvciBMaW51eCAoeDg2XzY0KQotLQot0tIER1bXAgY29tcGxldGVkIG9uIDIwMTktMDMtMjQgMTU6NTg6MDEK",
    "createdAt": "2019-03-25T08:43:48.000+0000",
    "updatedAt": "2019-03-25T08:57:53.000+0000",
    "primaryKey": {
      "id": 1
    }
  }
```

**Update File**

Update file according id.

| Property | Description |
| ------ | ------ |
| id | The unique identifier by which to identify the file |
| file | Multipart file  | 

**File Resource**

/api/v1/files/{id}

**PUT**

PUT /api/v1/files/{id}

```
 {
    "id": 1,
    "filename": "backup.sql",
    "type": "application/x-sql",
    "file": "LS0gTXlTUUwgZHVtcCAxMC4xMyAgRGlzdHJpYiA1LjcuMjUsIGZvciBMaW51eCAoeDg2XzY0KQotLQot0tIER1bXAgY29tcGxldGVkIG9uIDIwMTktMDMtMjQgMTU6NTg6MDEK",
    "createdAt": "2019-03-25T08:43:48.000+0000",
    "updatedAt": "2019-03-25T08:57:53.000+0000",
    "primaryKey": {
      "id": 1
    }
  }
```

**Delete File**

Delete file according id.

| Property | Description |
| ------ | ------ |
| id | The unique identifier by which to identify the file |

**File Resource**

/api/v1/files/{id}

**DELETE**

DELETE /api/v1/files/{id}

```{ "deleted": true}```


**Here I am showing how to install maven project into spring tool suite (sts) without docker dyrect into system.**

*  Install MYSQL Database
1.  According to OS, install database and setup security
2.  Set Username : root and Password:aniruddh
3.  Running host:localhost and port:3306
4.  Test database connection
5.  Create Database filestorage
6.  If you run project table automatically create ,if not created then create manually
```
CREATE TABLE `filestorage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `type` varchar(45) NOT NULL,
  `file` longblob NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;
```
*  Install Java 8
*  Install spring tool suite (sts)
*  Import project from git as maven project
*  Run as spring boot application
*  Test all end points which are given in document.

**Here I am showing how to run with docker.**


* First, clone the project and build locally 

``docker run --name fileStorageDemo-mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=filestorage -e MYSQL_USER=aniruddh -e MYSQL_PASSWORD=aniruddh -d mysql:5.6 ``

*  application.properties should be

```
spring.datasource.url = jdbc:mysql://mysql:3306/filestorage?useSSL=false
spring.datasource.username = root
spring.datasource.password = aniruddh
```

*  Check the log to make sure the server is running OK.

`docker logs  fileStorageDemo-mysql `

*  Run demo application in Docker container and link to demo-mysql.

`docker run -p 8080:8080 --name fileStorageDemo-app --link fileStorageDemo-mysql:mysql -d `



