FROM openjdk:8
ADD target/fileStorageDemo.jar fileStorageDemo.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "fileStorageDemo.jar"]