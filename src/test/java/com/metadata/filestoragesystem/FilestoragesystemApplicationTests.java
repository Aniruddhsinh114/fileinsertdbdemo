package com.metadata.filestoragesystem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FilestoragesystemApplicationTests {

	@Test
	public void contextLoads() {
	}

}
