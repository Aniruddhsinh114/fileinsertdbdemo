/**
 * 
 */
package com.metadata.filestoragesystem.Repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.metadata.filestoragesystem.Model.Filestorage;


/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
/**
* Test class for the File REST repository.
*
*
*/
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class FileRepositoryTest {

    @Autowired
    private FileRepository repository;

    @Test
    public void repositorySavesFileTest() {
    	byte[] testbytes= {'1','2','3','4','5'};
        Filestorage file =new Filestorage("testImage", "png", testbytes);
        Filestorage result = repository.save(file);
        assertEquals(result.getFilename(), "testImage");
        assertEquals(result.getType(), "png");
    }
    
  
}
