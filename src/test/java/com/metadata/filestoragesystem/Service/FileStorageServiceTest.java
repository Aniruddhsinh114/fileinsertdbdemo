/**
 * 
 */
package com.metadata.filestoragesystem.Service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.metadata.filestoragesystem.Model.Filestorage;
import com.metadata.filestoragesystem.Repository.FileRepository;

/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
/**
* Test class for the File REST service.
*
*
*/
public class FileStorageServiceTest {
	private FileRepository fileRepository;
	@Before
	public void prepare() {
		fileRepository=mock(FileRepository.class);
	}

	@Test
	public void storeFileTest() throws IOException {
		File file = new File("test.png");
		@SuppressWarnings("resource")
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile("test", "test", "image/png",
				input.toString().getBytes());
		Filestorage newFile = new Filestorage(multipartFile.getName(), multipartFile.getContentType(), multipartFile.getBytes());
		assertEquals(newFile.getFilename(), "test");
	}
	
	@Test
	public void findFilesTest() throws IOException {
		List<Filestorage> files = new ArrayList<Filestorage>();
		byte[] testbytes= {'1','2','3','4','5'};
        Filestorage file =new Filestorage("test", "image/png", testbytes);
        files.add(file);
        when(fileRepository.findAll()).thenReturn(files);
        assertEquals(fileRepository.findAll().get(0), file);
	}
	
	@Test
	public void deleteFileTest() throws IOException {
		File file = new File("test.png");
		@SuppressWarnings("resource")
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile("test", "test", "image/png",
				input.toString().getBytes());
		Filestorage newFile = new Filestorage(multipartFile.getName(), multipartFile.getContentType(), multipartFile.getBytes());
		fileRepository.delete(newFile);
		verify(fileRepository, times(1)).delete(newFile);
	}

}
