package com.metadata.filestoragesystem.API;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metadata.filestoragesystem.Model.Filestorage;
import com.metadata.filestoragesystem.Service.FileStorageService;
/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
/**
* Test class for the File REST controller.
*
*
*/

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private FileStorageService fileStorageService;

	private Filestorage filestorage;

	@Before
	public void prepare() throws IOException {
		File file = new File("test.png");
		@SuppressWarnings("resource")
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile("test", "test", "image/png", input.toString().getBytes());
		filestorage = new Filestorage(multipartFile.getName(), multipartFile.getContentType(),
				multipartFile.getBytes());
		filestorage.setId(1);

	}

	@Test
	public void testFindAll() throws Exception {
		List<Filestorage> files = new ArrayList<Filestorage>();
		files.add(filestorage);
		when(fileStorageService.getAllFiles()).thenReturn(files);
		this.mockMvc.perform(get("/api/v1/files")).andDo(print()).andExpect(status().isOk());

	}

	@Test
	public void testSingleFile() throws Exception {
		this.mockMvc
		.perform(MockMvcRequestBuilders.get("/api/v1/files/1").content(asJsonString(filestorage))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful());

	}

	@Test
	public void testStoreFile() throws Exception {

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/api/v1/files").content(asJsonString(filestorage))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void testUpdateFile() throws Exception {
		this.mockMvc
				.perform(MockMvcRequestBuilders.put("/api/v1/files/1").content(asJsonString(filestorage))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());

	}

	@Test
	public void testDeleteFile() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/files/{id}", 1))
				.andExpect(status().is2xxSuccessful());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
