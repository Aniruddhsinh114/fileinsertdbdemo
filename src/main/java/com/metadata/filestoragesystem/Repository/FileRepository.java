package com.metadata.filestoragesystem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.metadata.filestoragesystem.Model.Filestorage;



/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */

/**
 * Spring Data  repository for the file entity.
 */
public interface FileRepository extends JpaRepository<Filestorage, Integer> {

}

