/**
 * 
 */
package com.metadata.filestoragesystem.Utility;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
/**
 * Time format change class.
 */
public class TimeUtilities {
	public static Timestamp getTimeStamp() {
		return Timestamp.valueOf(LocalDateTime.now());
	}

}
