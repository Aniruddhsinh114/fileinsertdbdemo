/**
 * 
 */
package com.metadata.filestoragesystem.API;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
/**
 * @author aniruddhsinh
 *
 */
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.metadata.filestoragesystem.Exception.NullPointerException;
import com.metadata.filestoragesystem.Exception.ResourceNotFoundException;
import com.metadata.filestoragesystem.Model.Filestorage;
import com.metadata.filestoragesystem.Service.FileStorageService;

/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */

/**
 * REST controller for managing files.
 */
@RestController
@RequestMapping("/api/v1")
public class FileController {

	@Autowired
	private FileStorageService filestorageService;

	/**
	 * GET /files : get all files.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of files in body
	 */
	@GetMapping("/files")
	public List<Filestorage> getAllFiles() {
		return filestorageService.getAllFiles();
	}

	/**
	 * GET /filess/:id : get the "id" file.
	 *
	 * @param id the id of the file to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the file, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/files/{id}")
	public ResponseEntity<Filestorage> getFileById(@PathVariable(value = "id") Integer fileId)
			throws ResourceNotFoundException {
		return filestorageService.getFileById(fileId);
	}

	/**
	 * POST /files : Create a new file.
	 *
	 * @param file the file to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         file
	 * @throws ResourceNotFoundException if the resource not found.
	 * @throws IOException               if the byte convert exception
	 * @throws NullPointerException      if file is null
	 */
	@PostMapping("/files")
	public ResponseEntity<Filestorage> createFile(@Valid @RequestBody MultipartFile file)
			throws ResourceNotFoundException, IOException, NullPointerException {
		return filestorageService.storeFile(file);
	}

	/**
	 * PUT /files/:id : Updates an existing file.
	 *
	 * @param fileId the id of file to update
	 * @param file   the new file to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         file, or with status 400 (Bad Request) if the file is not valid, or
	 *         with status 500 (Internal Server Error) if the file couldn't be
	 *         updated
	 * @throws ResourceNotFoundException if the resource not found.
	 * @throws IOException               if the byte convert exception
	 * @throws NullPointerException      if file is null
	 */
	@PutMapping("/files/{id}")
	public ResponseEntity<Filestorage> updateFile(@PathVariable(value = "id") Integer fileId,
			@Valid @RequestBody MultipartFile file)
			throws ResourceNotFoundException, IOException, NullPointerException {
		return filestorageService.updateFile(fileId, file);
	}

	 /**
     * DELETE  /files/:id : delete the "id" file.
     *
     * @param id the id of the file to delete
     * @return the ResponseEntity with status 200 (OK)
     * @throws ResourceNotFoundException if the resource not found.
     */
	@DeleteMapping("/files/{id}")
	public Map<String, Boolean> deleteFile(@PathVariable(value = "id") Integer id) throws ResourceNotFoundException {
		return filestorageService.deleteFile(id);
	}

}
