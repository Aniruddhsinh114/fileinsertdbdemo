package com.metadata.filestoragesystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
@SpringBootApplication
public class FilestoragesystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilestoragesystemApplication.class, args);
	}

}
