package com.metadata.filestoragesystem.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.metadata.filestoragesystem.Exception.NullPointerException;
import com.metadata.filestoragesystem.Exception.ResourceNotFoundException;
import com.metadata.filestoragesystem.Model.Filestorage;
import com.metadata.filestoragesystem.Repository.FileRepository;
import com.metadata.filestoragesystem.Utility.TimeUtilities;

;

/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
/**
 * REST service for managing files.
 */
@Service
public class FileStorageService {

	@Autowired
	private FileRepository fileRepository;

	/**
	 * Create a new file.
	 *
	 * @param file the file to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         file
	 * @throws ResourceNotFoundException if the resource not found.
	 * @throws IOException               if the byte convert exception
	 * @throws NullPointerException      if file is null
	 */
	public ResponseEntity<Filestorage> storeFile(MultipartFile file) throws ResourceNotFoundException, IOException, NullPointerException{
		// Normalize file name
		if (file==null) {
			throw new NullPointerException("Sorry! File is null");
		}
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		if (fileName.contains("..")) {
			throw new ResourceNotFoundException("Sorry! Filename contains invalid path sequence " + fileName);
		}
		Filestorage newFile = new Filestorage(fileName, file.getContentType(), file.getBytes());
		final Filestorage newAddedFile = fileRepository.save(newFile);
		return ResponseEntity.ok(newAddedFile);
	}

	/**
	 * Updates an existing file.
	 *
	 * @param fileId the id of file to update
	 * @param file   the new file to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         file, or with status 400 (Bad Request) if the file is not valid, or
	 *         with status 500 (Internal Server Error) if the file couldn't be
	 *         updated
	 * @throws ResourceNotFoundException if the resource not found.
	 * @throws IOException               if the byte convert exception
	 * @throws NullPointerException      if file is null
	 */
	public ResponseEntity<Filestorage> updateFile(Integer fileId, MultipartFile fileDetails)
			throws ResourceNotFoundException, IOException, NullPointerException {
		if (fileDetails==null) {
			throw new NullPointerException("Sorry! File is null");
		}
		String fileName = StringUtils.cleanPath(fileDetails.getOriginalFilename());
		if (fileName.contains("..")) {
			throw new ResourceNotFoundException("Sorry! Filename contains invalid path sequence " + fileName);
		}
		Filestorage file = fileRepository.findById(fileId)
				.orElseThrow(() -> new ResourceNotFoundException("File not found :: " + fileId));
		file.setFilename(fileName);
		file.setType(fileDetails.getContentType());
		file.setFile(fileDetails.getBytes());
		file.setUpdatedAt(TimeUtilities.getTimeStamp());
		final Filestorage updatedFile = fileRepository.save(file);
		return ResponseEntity.ok(updatedFile);
	}

	/**
	 * get all files.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of files in body
	 */
	public List<Filestorage> getAllFiles() {
		return fileRepository.findAll();
	}

	/**
	 * get the "id" file.
	 *
	 * @param id the id of the file to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the file, or
	 *         with status 404 (Not Found)
	 */
	public ResponseEntity<Filestorage> getFileById(Integer fileId) throws ResourceNotFoundException {
		Filestorage fileFromStorage = fileRepository.findById(fileId)
				.orElseThrow(() -> new ResourceNotFoundException("File not found :: " + fileId));
		return ResponseEntity.ok().body(fileFromStorage);
	}
	
	/**
     * delete the "id" file.
     *
     * @param id the id of the file to delete
     * @return the ResponseEntity with status 200 (OK)
     * @throws ResourceNotFoundException if the resource not found.
     */
	public Map<String, Boolean> deleteFile(Integer id) throws ResourceNotFoundException {
		Filestorage file = fileRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("File not found :: " + id));

		fileRepository.delete(file);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
