package com.metadata.filestoragesystem.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author ANIRUDDHSINH CHAVDA
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NullPointerException extends Exception{

	private static final long serialVersionUID = 1L;

	public NullPointerException(String message){
    	super(message);
    }
}